#!/usr/bin/bash

KBACKLIGHT_MAKE=":white:kbd_backlight"
BRIGHTNESS=$(cat /sys/class/leds/$KBACKLIGHT_MAKE/brightness)
MAX_BRIGHTNESS=$(cat /sys/class/leds/$KBACKLIGHT_MAKE/max_brightness)
ICON="/usr/share/icons/HighContrast/256x256/status/keyboard-brightness.png"

if [ "$BRIGHTNESS" -lt "$MAX_BRIGHTNESS" ]
then
	NEW_BRIGHTNESS=$(awk "BEGIN {print $BRIGHTNESS+$MAX_BRIGHTNESS*0.05; exit}")
else
	NEW_BRIGHTNESS=$MAX_BRIGHTNESS
fi

BRIGHTNESS_PERCENT=$(echo "$NEW_BRIGHTNESS / $MAX_BRIGHTNESS * 100 / 1" | bc -l )
echo $NEW_BRIGHTNESS > /sys/class/leds/$KBACKLIGHT_MAKE/brightness
notify-send "Keyboard Brightness: ${BRIGHTNESS_PERCENT%.*}%" -i "$ICON" -h "string:synchronous:keyboard_brightness" -h "int:value:$BRIGHTNESS_PERCENT"